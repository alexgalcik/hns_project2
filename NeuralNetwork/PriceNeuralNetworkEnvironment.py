import os
import shutil
import joblib
import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt

from datetime import datetime
from keras.layers import LSTM, Bidirectional, Dropout, Dense, GRU
from keras.models import Sequential
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from Db.Manager.InputSampleDatabaseManager import InputSampleDatabaseManager
from Modules.Api.DatasetApiFormatter import DatasetApiFormatter
from Scripts.DataScripts.find_file_path import get_project_root, get_path


class PriceNeuralNetworkEnvironment:

    def __init__(self):
        self.physical_devices = tf.config.list_physical_devices("GPU")
        try:
            tf.config.experimental.set_memory_growth(self.physical_devices[0], True)
        except:
            pass

        self.model = Sequential()
        # raw input and output data
        self.train_data = []
        self.test_data = []
        self.price_data = []
        # scaled input and output data for neural network
        self.x_train = []
        self.y_train = []
        self.x_test = []
        self.y_test = []
        # scaled data
        self.scaled_targets = []
        self.scaled_train_data = []
        self.scaled_test_data = []
        # SCALERS for inputs and targets
        self.input_sc = StandardScaler()
        self.target_sc = StandardScaler()
        # test params
        self.dead_zone_buy = 0.6
        self.dead_zone_sell = 0.6
        self.reverse = True
        self.action = False
        self.time_to_predict = 30
        self.pair = None
        self.dataframe = None
        # nn settings
        self.window_size = 50
        self.future_predict = 1
        self.history = None
        self.new_date_to = None
        # inputs
        # 'bb_upper',
        # 'bb_lower',
        self.inputs = ['close_open_diff',
                       'close',
                       'open',
                       'ema_short',
                       'ema_long',
                       'ema_short_ema_long_diff',
                       'gradient',
                       'macd',
                       'momentum',
                       'rsi',
                       'signal',
                       'volume',
                       'close']

    def set_window_size(self, window_size):
        self.window_size = window_size

    def load_dataframe(self):
        path = get_path('Download1')
        dir_list = os.listdir(path)
        df_list = []
        for file in dir_list:
            df = pd.read_csv(path + '/' + file)
            df_list.append(df)
        df_list = pd.concat(df_list, axis=0)
        datasetFormatter = DatasetApiFormatter(df_list)
        datasetFormatter.dataset = datasetFormatter.format_dataframe(datasetFormatter.dataset)
        datasetFormatter.add_statistic_indicators()
        df_list = datasetFormatter.dataset
        self.dataframe = df_list

    def preprocess_input_data(self, train_ratio):
        # turn off warning
        pd.options.mode.chained_assignment = None  # default='warn'
        # self.dataframe = get_boilinger_bands(self.dataframe).dropna()
        # count train ratio
        all_rows = len(self.dataframe.index)
        train_index = int(train_ratio * all_rows)
        self.new_date_to = self.dataframe.iloc[train_index]['date'].strftime('%d.%m.%Y')
        # spliting data
        self.train_data = self.dataframe.iloc[:train_index, :]
        self.train_data = self.train_data[self.inputs]
        self.test_data = self.dataframe.iloc[train_index:, :]
        self.test_data = self.test_data[self.inputs]
        self.price_data = self.dataframe.iloc[train_index:, 5].reset_index(drop=True)

    def prepare_train_data(self):
        self.scaled_train_data = self.input_sc.fit_transform(
            self.train_data.iloc[:, :(self.train_data.shape[1] - 1)].values)
        self.scaled_targets = self.target_sc.fit_transform(
            self.train_data.iloc[:, (self.train_data.shape[1] - 1):self.train_data.shape[1]].values)

        for i in range(self.window_size, len(self.scaled_train_data) - self.future_predict + 1):
            self.x_train.append(self.scaled_train_data[i - self.window_size:i, 0: self.train_data.shape[1] - 1])
            self.y_train.append(self.scaled_targets[i, 0])

        self.x_train, self.y_train = np.array(self.x_train), np.array(self.y_train)
        self.x_train = np.reshape(self.x_train, (self.x_train.shape[0], self.x_train.shape[1], self.x_train.shape[2]))

    def prepare_test_evaluation_data(self):
        self.scaled_test_data = self.input_sc.transform(self.test_data.iloc[:, :(self.test_data.shape[1] - 1)].values)
        self.scaled_targets = self.target_sc.transform(
            self.test_data.iloc[:, (self.test_data.shape[1] - 1):self.test_data.shape[1]].values)

        for i in range(self.window_size, len(self.scaled_test_data) - self.future_predict + 1):
            self.x_test.append(self.scaled_test_data[i - self.window_size:i, 0: self.test_data.shape[1] - 1])
            self.y_test.append(self.scaled_targets[i, 0])

        self.x_test, self.y_test = np.array(self.x_test), np.array(self.y_test)
        self.x_test = np.reshape(self.x_test, (self.x_test.shape[0], self.x_test.shape[1], self.x_test.shape[2]))

    def save_scalers(self, enabled=False):
        if enabled:
            return self.save_scaler(self.input_sc, input_scaler=True), self.save_scaler(self.target_sc,
                                                                                        input_scaler=False)

    def save_scaler(self, scaler, input_scaler=True):
        root_path = str(get_project_root())
        if input_scaler:
            name = datetime.now().strftime("%Y-%m-%d-%H-%M-") + 'input-scaler'
            model_folder_path = root_path + '/NeuralNetwork/scaler_files/input_scaler'
        else:
            name = datetime.now().strftime("%Y-%m-%d-%H-%M-") + 'target-scaler'
            model_folder_path = root_path + '/NeuralNetwork/scaler_files/target_scaler'

        if not os.path.exists(model_folder_path):
            os.makedirs(model_folder_path)
        name = name + '.gz'
        file_name = os.path.join(model_folder_path, name)
        joblib.dump(scaler, file_name)
        print('Scaler: ' + name + ' has been saved successfully.')

    def load_scaler(self, scaler_name, input_scaler=True):
        root_path = str(get_project_root())
        name = scaler_name
        if input_scaler:
            model_folder_path = root_path + '/NeuralNetwork/scaler_files/input_scaler'
            file_name = os.path.join(model_folder_path, name)
            self.input_sc = joblib.load(file_name)
        else:
            model_folder_path = root_path + '/NeuralNetwork/scaler_files/target_scaler'
            file_name = os.path.join(model_folder_path, name)
            self.target_sc = joblib.load(file_name)

        print('Scaler: ' + name + ' has been loaded successfully.')

    def create_model(self):
        self.model.add(
            LSTM(units=128, activation='relu', return_sequences=True,
                 input_shape=(self.x_train.shape[1], self.x_train.shape[2])))
        self.model.add(LSTM(units=512, return_sequences=True))
        self.model.add(Dropout(0.1))
        self.model.add(LSTM(units=1024, return_sequences=True))
        self.model.add(Dropout(0.1))
        self.model.add(LSTM(units=128, return_sequences=True))
        self.model.add(LSTM(units=64))
        self.model.add(Dense(units=1))
        adam = tf.optimizers.Adam()
        self.model.compile(optimizer=adam, loss='mse')
        self.model.summary()

    def train(self, epochs=10, validation_split=0.2, shuffle=True, batch_size=256, show_graph=False):
        if self.model is None:
            self.create_model()

        # earlystopping = tf.keras.callbacks.EarlyStopping(
        #     monitor="val_loss",
        #     patience=20,
        # )

        self.history = self.model.fit(self.x_train,
                                      self.y_train,
                                      epochs=epochs,
                                      batch_size=batch_size,
                                      validation_split=validation_split,
                                      shuffle=shuffle)
        # callbacks=[earlystopping])
        if show_graph:
            plt.figure()
            plt.plot(self.history.history['loss'], label='training')
            plt.plot(self.history.history['val_loss'], label='val_loss')
            plt.xlabel('Epoch')
            plt.ylabel('Loss')
            plt.legend(loc='lower right')
            plt.show()

    def evaluate_test(self):
        score = self.model.evaluate(self.x_test, self.y_test, batch_size=self.window_size + self.future_predict)
        print('Evaluate Test Summary:', score)

    def real_test(self):
        outputs = self.model_predict_array(self.x_test)
        self.plot_test(outputs, self.price_data)

    def model_predict_with_plot(self):
        scaled_prediction = self.model.predict(self.x_test)
        inversed_prediction = self.target_sc.inverse_transform(scaled_prediction)
        # print(scaled_prediction.shape)
        # print(scaled_prediction)
        plt.plot(self.target_sc.inverse_transform(self.y_test.reshape(-1, 1)), color='black', label='Actual signal')
        plt.plot(inversed_prediction, color='green', label='Predicted Signal')
        plt.title('Stock Signal Prediction')
        plt.xlabel('Time')
        plt.ylabel(self.pair)
        plt.legend()
        plt.show()

    def show_accuracy_plot(self, title='model accuracy'):
        plt.plot(self.history.history['accuracy'])
        plt.plot(self.history.history['val_accuracy'])
        plt.title(title)
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'val'], loc='upper left')
        plt.show()

    def show_loss_plot(self, title='model loss'):
        plt.plot(self.history.history['loss'])
        plt.plot(self.history.history['val_loss'])
        plt.title(title)
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train data', 'validation data'], loc='upper left')
        plt.show()

    def plot_test(self, outputs, price):
        # TODO len pre test normalne to musi byt v inej classe
        price = price.iloc[self.window_size:].reset_index(drop=True)
        plt.plot(outputs, color='b', label='predicted')
        plt.plot(price, color='r', label='real')
        plt.xlabel("time samples")
        plt.ylabel("price")
        plt.title("Market")
        plt.legend()
        plt.show()

    def model_predict(self, input):
        input = np.reshape(input, (1, self.window_size, len(self.inputs) - 1))
        scaled_prediction = self.model.predict(input)
        return self.target_sc.inverse_transform(scaled_prediction)

    def model_predict_array(self, array):
        scaled_prediction = self.model.predict(array)
        return self.target_sc.inverse_transform(scaled_prediction)

    def prepare_input(self, dataframe):
        x_test = []
        dataframe = dataframe[self.inputs]
        scaled_test_data = self.input_sc.transform(dataframe.iloc[:, :(dataframe.shape[1] - 1)].values)
        x_test.append(scaled_test_data[(len(scaled_test_data) - 1 - self.window_size):(len(scaled_test_data) - 1),
                      0: dataframe.shape[1]])

        return np.array(x_test)

    def save_tensorflow_model(self):
        name = datetime.now().strftime("%Y-%m-%d-%H-%M-") + self.pair
        root_path = str(get_project_root())
        model_folder_path = root_path + '/NeuralNetwork/model_file/'
        if not os.path.exists(model_folder_path):
            os.makedirs(model_folder_path)
        file_name = os.path.join(model_folder_path, name)
        self.model.save(file_name)
        shutil.make_archive(file_name, 'zip', file_name)
        print('Model: ' + name + ' saved successfully.')
        return name

    def load_tensorflow_model(self, model_name):
        root_path = str(get_project_root())
        model_folder_path = root_path + '/NeuralNetwork/model_file/'
        file_name = os.path.join(model_folder_path, model_name)
        shutil.unpack_archive(file_name + '.zip', file_name + '/', 'zip')
        self.model = tf.keras.models.load_model(file_name)
        print('Model: ' + model_name + ' load successfully.')
