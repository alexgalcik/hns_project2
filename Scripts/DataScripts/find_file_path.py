import os
from pathlib import Path


def get_project_root() -> Path:
    return Path(__file__).parent.parent.parent


def get_path(file):
    for root, dirs, files in os.walk(get_project_root()):
        for name in files:
            if name == file:
                return os.path.abspath(os.path.join(root, name))
        for name in dirs:
            if name == file:
                return os.path.abspath(os.path.join(root, name))

# TEST
# print(get_project_root())  # C:\Development\AI_Trading
# print(get_path('data.json'))  # C:\Development\AI_Trading\data.json
# print(get_path('Furrier.py'))  # C:\Development\AI_Trading\Modules\Services\Furrier.py
# print(get_path('target.py'))  # C:\Development\AI_Trading\Db\Model\target.py
