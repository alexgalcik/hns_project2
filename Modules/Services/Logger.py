import os

from Scripts.DataScripts.find_file_path import get_path
from datetime import date
from termcolor import colored


class Logger:

    @staticmethod
    def info(*args, **kwargs: bool):
        string = []
        d1 = date.today().strftime("%d/%m/%Y")

        for arg in args:
            string.append(arg)

        for key, value in kwargs.items():
            if key == "toFile" or key == "to_file":
                to_file = value

        msg = "".join([str(item) for item in string])

        print("[%s] INFO: %s " % (d1, msg))

        if to_file:
            path = os.path.join(get_path("Logs"), 'project_log.txt')
            if not os.path.exists(path):
                open(path, 'w').close()

            file = open(get_path("project_log.txt"), "a")
            file.writelines("[%s] INFO: %s \n" % (d1, msg))
            file.close()

    @staticmethod
    def warning(*args, **kwargs: bool):
        string = []
        d1 = date.today().strftime("%d/%m/%Y")

        for arg in args:
            string.append(arg)

        for key, value in kwargs.items():
            if key == "toFile" or key == "to_file":
                to_file = value

        msg = "".join([str(item) for item in string])

        print(colored("[%s] WARNING: %s " % (d1, msg), 'red'))

        if to_file:
            path = os.path.join(get_path("Logs"), 'project_log.txt')
            if not os.path.exists(path):
                open(path, 'w').close()

            file = open(get_path("project_log.txt"), "a")
            file.writelines("[%s] WARNING: %s \n" % (d1, msg))
            file.close()

    @staticmethod
    def msg(*args):
        string = []
        for arg in args:
            string.append(arg)

        msg = "".join([str(item) for item in string])

        print(colored(" %s" % msg, 'blue'))

    @staticmethod
    def input(*args):
        string = []
        for arg in args:
            string.append(arg)

        msg = "".join([str(item) for item in string])

        return input(colored(" %s " % msg, 'green'))

    # Print iterations progress
    @staticmethod
    def progress_bar(iterable, total, prefix='', suffix='', decimals=1, length=100, fill='█', print_end=""):
        """
        Call in a loop to create terminal progress bar
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
            printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
        """

        # Progress Bar Printing Function
        def print_progress_bar(iteration):
            percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
            filledLength = int(length * iteration // total)
            bar = fill * filledLength + '-' * (length - filledLength)
            print(colored(f'\r{prefix} |{bar}| {percent}% {suffix}', 'blue'), end=print_end)

        # Initial Call
        print_progress_bar(0)
        # Update Progress Bar
        for i, item in enumerate(iterable):
            yield item
            print_progress_bar(i + 1)
        # Print New Line on Complete
        print()
