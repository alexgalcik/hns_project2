import pandas as pd
from datetime import datetime
import numpy as np


class InputSampleServices:
    @staticmethod
    def check_missing_days(dataframe, date_from=None, date_to=None):
        dates_loaded = dataframe['csv_name'].unique().tolist()
        dates_loaded[:] = [s.partition('.')[0] for s in dates_loaded]
        dates_loaded = sorted(dates_loaded, key=lambda x: datetime.strptime(x, '%Y-%m-%d'))
        dates_wanted = pd.date_range(date_from, date_to, freq='D')
        dates_wanted = pd.Series(dates_wanted.format()).tolist()

        diff = list(set(dates_wanted) - set(dates_loaded))
        missing_days = sorted(diff, key=lambda x: datetime.strptime(x, '%Y-%m-%d'))
        return missing_days

    @staticmethod
    def parse_dataframe_to_single_days(dataframe):
        if dataframe is not None:
            dataframes_by_day = [group[1] for group in dataframe.groupby(dataframe.csv_name)]
        else:
            return None, None, None
        return dataframes_by_day

    @staticmethod
    def check_dates_without_target(dataframe):
        dataframes_by_days = InputSampleServices.parse_dataframe_to_single_days(dataframe)
        dates = []
        for df in dataframes_by_days:
            for index, contents in df.iterrows():
                if np.isnan(contents['target']):
                    dates.append(contents['csv_name'])
                    break
        return dates
