import configparser
from datetime import datetime
import time

from binance import Client

from Modules.Api.DatasetApiFormatter import DatasetApiFormatter

import numpy as np
import pandas as pd
import datetime as dt
from Scripts.DataScripts.find_file_path import get_path


class BinanceApi:

    def __init__(self):
        config = configparser.RawConfigParser()
        config.read(get_path('application.properties'))
        properties = dict(config.items('Binance'))
        # Creating client of BINANCE API with our API_KEY and API_SECRET. It is static variable of this class
        self.binance_client = Client('', '')

    # Function for getting dataset into memory for specified number of hours and with specified interval, default is 5m
    def get_dataset(self, hours, interval, pair):
        now = datetime.now()
        if (now.minute % 5 == 4) and now.second > 30:
            time.sleep(40)

        if interval == '1':
            raw_data = self.binance_client.get_historical_klines(pair, Client.KLINE_INTERVAL_1MINUTE,
                                                                 "{} hours ago UTC".format(hours))
        elif interval == '5':
            raw_data = self.binance_client.get_historical_klines(pair, Client.KLINE_INTERVAL_5MINUTE,
                                                                 "{} hours ago UTC".format(hours))
            pass
        elif interval == '30':
            raw_data = self.binance_client.get_historical_klines(pair, Client.KLINE_INTERVAL_30MINUTE, "{} hours ago "
                                                                                                       "UTC".format(
                hours))
            pass
        elif interval == '60':
            raw_data = self.binance_client.get_historical_klines(pair, Client.KLINE_INTERVAL_1HOUR, "{} hours ago "
                                                                                                    "UTC".format(
                hours))
            pass
        elif interval == '1d':
            raw_data = self.binance_client.get_historical_klines(pair, Client.KLINE_INTERVAL_1DAY, "{} hours ago "
                                                                                                   "UTC".format(
                hours))
            pass
        elif interval == '1w':
            raw_data = self.binance_client.get_historical_klines(pair, Client.KLINE_INTERVAL_1WEEK, "{} hours ago "
                                                                                                    "UTC".format(
                hours))
            pass
        elif interval == '1m':
            raw_data = self.binance_client.get_historical_klines(pair, Client.KLINE_INTERVAL_1MONTH, "{} hours ago "
                                                                                                     "UTC".format(
                hours))
            pass
        else:
            raise ValueError("Only '1m' or '5m' interval is supported to this day.")

        raw_data = np.array(raw_data)

        dataset = pd.DataFrame(raw_data.reshape(-1, 12), dtype=float, columns=('date',
                                                                               'open', 'high', 'low', 'close',
                                                                               'volume',
                                                                               'close_time', 'qav',
                                                                               'num_trades',
                                                                               'taker_base_vol',
                                                                               'taker_quote_vol', 'ignore'))
        dataset['pair'] = pair
        dataset['date'] = dataset['date'] / 1000
        dataset['date'] = [dt.datetime.fromtimestamp(x) for x in dataset['date']]

        del dataset['close_time']
        del dataset['qav']
        del dataset['num_trades']
        del dataset['taker_base_vol']
        del dataset['taker_quote_vol']
        del dataset['ignore']

        datasetAPi = DatasetApiFormatter(dataset)
        datasetAPi.add_statistic_indicators(ema_short=3, ema_long=6, macd_slow=12, macd_fast=26, macd_signal=9, rsi_n=6)

        return datasetAPi.dataset

    def time_convert(self, sec):
        mins = sec // 60
        sec = sec % 60
        hours = mins // 60
        mins = mins % 60
        print("Time Lapsed after while: {0}:{1}:{2}".format(int(hours), int(mins), sec))

    # Function for getting actual price of coin for live prediction
    def get_actual_price(self, pair):
        return float(self.binance_client.get_symbol_ticker(symbol=pair)['price'])

    # Function for getting actual candlestick data
    def get_recent_ohlc(self, dataset, interval, pair):
        # dataset is passed by reference, no need to return
        if interval == '1':
            raw_data = self.binance_client.get_historical_klines(pair, Client.KLINE_INTERVAL_1MINUTE,
                                                                 "1 minute ago UTC")
        elif interval == '5':
            raw_data = self.binance_client.get_historical_klines(pair, Client.KLINE_INTERVAL_5MINUTE,
                                                                 "5 minutes ago UTC")
            start_time = None
            if raw_data is None or len(raw_data) <= 0:
                print("Raw data is empty starting timer")
                start_time = time.time()
            while raw_data is None or len(raw_data) <= 0:
                raw_data = self.binance_client.get_historical_klines(pair, Client.KLINE_INTERVAL_5MINUTE,
                                                                     "5 minutes ago UTC")
            if start_time is not None:
                end_time = time.time()
                lapsed_time = end_time - start_time
                self.time_convert(lapsed_time)
        else:
            raw_data = []

        raw_data = np.array(raw_data)

        recent_trade = pd.DataFrame(raw_data.reshape(-1, 12), dtype=float, columns=('date',
                                                                                    'open', 'high', 'low', 'close',
                                                                                    'volume',
                                                                                    'close_time', 'qav',
                                                                                    'num_trades',
                                                                                    'taker_base_vol',
                                                                                    'taker_quote_vol', 'ignore'))

        recent_trade['pair'] = pair
        recent_trade['date'] = recent_trade['date'] / 1000
        recent_trade['date'] = [dt.datetime.fromtimestamp(x) for x in recent_trade['date']]

        del recent_trade['close_time']
        del recent_trade['qav']
        del recent_trade['num_trades']
        del recent_trade['taker_base_vol']
        del recent_trade['taker_quote_vol']
        del recent_trade['ignore']

        # recent_trade = DatasetApiFormatter.format_dataframe(recent_trade)
        dataset = dataset.append(recent_trade, ignore_index=True)
        dataset = dataset.drop(dataset.head(1).index)
        datasetAPi = DatasetApiFormatter(dataset)
        datasetAPi.dataset = datasetAPi.format_dataframe(dataset)
        datasetAPi.add_statistic_indicators(ema_short=3, ema_long=6, macd_slow=12,
                                            macd_fast=26, macd_signal=9, rsi_n=6)

        return datasetAPi.dataset
