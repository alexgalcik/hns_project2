import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler


class DatasetApiFormatter:
    def __init__(self, dataset):
        self.dataset = dataset
        self.indicators = []

    def add_statistic_indicators(self, ema_short=3, ema_long=6, macd_slow=12, macd_fast=26, macd_signal=9, rsi_n=6,
                                 vwap_n=14):
        if ema_short > ema_long:
            raise Exception("Short ema must be lower than long ema")
        if ema_long < ema_short:
            raise Exception("Long ema must be higher than short ema")

        self.dataset['close_open_diff'] = self.dataset['close'] - self.dataset['open']
        self.__ema(ema_short, 'short')
        self.__ema(ema_long, 'long')
        self.dataset['ema_short_ema_long_diff'] = self.dataset['ema_short'] - self.dataset['ema_long']

        self.__macd(macd_slow, macd_fast, macd_signal)
        self.__rsi(rsi_n)
        self.__gradient()

    def __ema(self, n, type_string):
        ema_name = None
        if type_string == 'short':
            ema_name = 'ema_short'
        elif type_string == 'long':
            ema_name = 'ema_long'
        self.dataset[ema_name] = self.dataset['close'].ewm(span=n, adjust=False).mean()
        self.indicators.append(ema_name)

    def __macd(self, nslow, nfast, nsignal):
        macd_name = ['macd', 'signal', 'momentum']
        closed = self.dataset['close']
        ema_slw = closed.ewm(span=nslow, min_periods=1, adjust=False).mean()
        ema_fst = closed.ewm(span=nfast, min_periods=1, adjust=False).mean()
        macd = ema_slw - ema_fst
        signal = macd.ewm(span=nsignal, min_periods=1, adjust=False).mean()
        momentum = macd - signal
        self.dataset[macd_name[0]] = macd
        self.dataset[macd_name[1]] = signal
        self.dataset[macd_name[2]] = momentum
        self.indicators.append(macd_name[0])

    def __vwap(self, n):
        vwap_name = 'vwap'
        self.dataset[vwap_name] = 0
        v = self.dataset.iloc[:, self.dataset.columns.get_loc('volume')]
        h = self.dataset.iloc[:, self.dataset.columns.get_loc('high')]
        l = self.dataset.iloc[:, self.dataset.columns.get_loc('low')]

        vwap = np.cumsum(v * (h + l) / 2) / np.cumsum(v)
        self.dataset[vwap_name] = vwap
        self.indicators.append(vwap_name)

    def __rsi(self, n):
        rsi_name = 'rsi'
        closed = self.dataset['close']
        deltas = np.diff(closed)
        seed = deltas[:n + 1]
        up = seed[seed >= 0].sum() / n
        down = -seed[seed < 0].sum() / n
        rs = up / down
        rsi = np.zeros_like(closed)
        rsi[:n] = 100. - 100. / (1. + rs)

        for i in range(n, len(closed)):
            delta = deltas[i - 1]  # cause the diff is 1 shorter
            if delta > 0:
                upval = delta
                downval = 0.
            else:
                upval = 0.
                downval = -delta
            up = (up * (n - 1) + upval) / n
            down = (down * (n - 1) + downval) / n
            rs = up / down
            rsi[i] = 100. - 100. / (1. + rs)
        self.dataset[rsi_name] = rsi
        self.indicators.append(rsi_name)

    def __gradient(self):
        gradient_name = 'gradient'
        self.dataset[gradient_name] = [i * 100 for i in np.gradient(self.dataset['close'])]
        self.indicators.append(gradient_name)

    @staticmethod
    def format_dataframe(pd_dtf):
        pd_dtf['open'] = pd_dtf['open'].astype('double')
        pd_dtf['high'] = pd_dtf['high'].astype('double')
        pd_dtf['low'] = pd_dtf['low'].astype('double')
        pd_dtf['close'] = pd_dtf['close'].astype('double')
        pd_dtf['volume'] = pd_dtf['volume'].astype('double')
        pd_dtf['date'] = pd_dtf['date'].astype('datetime64')
        pd_dtf.index = pd.to_datetime(pd_dtf['date'])
        return pd_dtf
