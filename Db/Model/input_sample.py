from sqlalchemy import Column, Integer, String, ForeignKey, Float, Time
from sqlalchemy.orm import relation
from Db.Base import Base
from Db.Model.target import Target


class InputSample(Base):
    __tablename__ = 'input_sample'
    id = Column(Integer, primary_key=True, autoincrement=True)
    date = Column(Time)
    open = Column(Float)
    high = Column(Float)
    low = Column(Float)
    close = Column(Float)
    volume = Column(Float)
    close_open_diff = Column(Float)
    ema_short = Column(Float)
    ema_long = Column(Float)
    ema_short_ema_long_diff = Column(Float)
    macd = Column(Float)
    signal = Column(Float)
    momentum = Column(Float)
    rsi = Column(Float)
    gradient = Column(Float)
    target = Column(Integer, ForeignKey('target.id'))
    csv_name = Column(String)
    pair = Column(String)

    target_obj = relation(Target)  # child object relation it is used in view example
