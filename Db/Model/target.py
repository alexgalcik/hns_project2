from sqlalchemy import Column, Integer, String, Float
from Db.Base import Base


class Target(Base):
    __tablename__ = 'target'
    id = Column(Integer, primary_key=True, autoincrement=True)
    buy = Column(Float)
    sell = Column(Float)
    signal = Column(String)
