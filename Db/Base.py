import configparser
from sqlalchemy.ext.declarative import declarative_base
from Scripts.DataScripts.find_file_path import get_path

# get configuration properties
config = configparser.RawConfigParser()
config.read(get_path('application.properties'))
properties = dict(config.items('Database'))
# set DATABASE scheme
Base = declarative_base()
Base.metadata.schema = properties['database.schema']
