import configparser
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from Db.Base import Base
from Db.Model.input_sample import InputSample
import pandas as pd

from Modules.Api.DatasetApiFormatter import DatasetApiFormatter
from Modules.Services.Logger import Logger
from Scripts.DataScripts.find_file_path import get_path


class InputSampleDatabaseManager:

    def __init__(self):
        config = configparser.RawConfigParser()
        config.read(get_path('application.properties'))
        properties = dict(config.items('Database'))
        self.engine = create_engine(
            properties['database.type'] + "+" + properties['database.connector'] + "://" + properties[
                'database.user'] + ":" + properties['database.password'] + "@" + properties[
                'database.host'] + ":" +
            properties['database.port'] + "/" + properties['database.name'])
        self.cursor = self.engine.connect()
        self.session = None

    def open_session(self):
        Base.metadata.create_all(self.engine)
        Session = sessionmaker(self.engine)
        session = Session()
        self.session = session
        return session

    def get_input_sample(self, id_input_sample):
        session = self.open_session()
        result = session.query(InputSample).filter(InputSample.id == id_input_sample)
        session.commit()
        session.close()
        return result

    def get_input_samples_by_pair(self, pair):
        session = self.open_session()
        result = session.query(InputSample).filter(InputSample.pair == pair)
        session.commit()
        session.close()
        return result

    def get_pairs_in_db(self):
        session = self.open_session()
        result = session.query(InputSample.pair).distinct()
        session.commit()
        session.close()
        pairs = []
        for pair in result:
            pairs.append(pair[0])
        return pairs

    def insert_input_sample_for_day(self, dataframe: pd.DataFrame):
        session = self.open_session()
        csv_name = str(dataframe['date'][0])
        csv_name = csv_name.split(" ", 1)
        csv_name = csv_name[0] + ".csv"
        for i in range(len(dataframe)):
            input_sample = self.get_input_sample_from_dataframe_row(dataframe.iloc[i], csv_name)
            session.add(input_sample)
        session.commit()
        session.close()

    def insert_input_sample_for_n_days(self, dataframes):
        session = self.open_session()
        for dataframe in dataframes:
            csv_name = str(dataframe['date'][0])
            csv_name = csv_name.split(" ", 1)
            csv_name = csv_name[0] + ".csv"
            for i in range(len(dataframe)):
                input_sample = self.get_input_sample_from_dataframe_row(dataframe.iloc[i], csv_name)
                session.add(input_sample)
        session.commit()
        session.close()

    @staticmethod
    def get_input_sample_from_dataframe_row(dataframe_row, csv_name):
        input_sample = InputSample()
        input_sample.date = dataframe_row['date']
        input_sample.open = dataframe_row['open']
        input_sample.high = dataframe_row['high']
        input_sample.low = dataframe_row['low']
        input_sample.close = dataframe_row['close']
        input_sample.volume = dataframe_row['volume']
        input_sample.close_open_diff = dataframe_row['close_open_diff']
        input_sample.ema_short = dataframe_row['ema_short']
        input_sample.ema_long = dataframe_row['ema_long']
        input_sample.ema_short_ema_long_diff = dataframe_row['ema_sort_ema_long_diff']
        input_sample.macd = dataframe_row['macd']
        input_sample.signal = dataframe_row['signal']
        input_sample.momentum = dataframe_row['momentum']
        input_sample.rsi = dataframe_row['rsi']
        input_sample.gradient = dataframe_row['gradient']
        input_sample.pair = dataframe_row['pair']
        # input_sample.target = target[i]
        input_sample.csv_name = csv_name

        return input_sample

    def get_dataframe_from_db(self, currency_pair, query_all=True, date_from=None, date_to=None):
        session = self.open_session()

        if query_all:
            dataframe = pd.read_sql(sql=session.query(InputSample).filter(InputSample.pair == currency_pair).order_by(
                InputSample.date).statement,
                                    con=session.bind)
            dataframe = DatasetApiFormatter.format_dataframe(dataframe)
            result = session.query(InputSample).filter(InputSample.pair == currency_pair).order_by(InputSample.date)
            session.commit()
            Logger.msg("All data was transformed into Pandas DataFrame")
        else:
            dataframe = pd.read_sql(sql=session.query(InputSample).filter(InputSample.pair == currency_pair,
                                                                          InputSample.date.between(date_from,
                                                                                                   date_to)).order_by(
                InputSample.date).statement, con=session.bind)

            dataframe = DatasetApiFormatter.format_dataframe(dataframe)
            result = session.query(InputSample).filter(InputSample.pair == currency_pair,
                                                       InputSample.date.between(date_from, date_to)).order_by(
                InputSample.date)

            session.commit()
            Logger.msg("Data for selected interval was transformed into Pandas DataFrame")

        if dataframe.empty:
            return None, None

        return dataframe, result

    def get_train_dataframe_from_db(self, currency_pair, query_all=True, date_from=None, date_to=None):
        session = self.open_session()

        if query_all:
            dataframe = pd.read_sql(sql=session.query(InputSample).filter(InputSample.pair == currency_pair).order_by(
                InputSample.date).statement,
                                    con=session.bind)
            dataframe = DatasetApiFormatter.format_dataframe(dataframe)
            session.commit()
            Logger.msg("All data was transformed into Pandas DataFrame...")
        else:
            dataframe = pd.read_sql(sql=session.query(InputSample).filter(InputSample.pair == currency_pair,
                                                                          InputSample.date.between(date_from,
                                                                                                   date_to)).order_by(
                InputSample.date).statement, con=session.bind)

            dataframe = DatasetApiFormatter.format_dataframe(dataframe)
            session.commit()
            Logger.msg("Data for selected interval was transformed into Pandas DataFrame")

        if dataframe.empty:
            return None

        return dataframe

    def close_connection(self):
        self.cursor.close()
        self.engine.dispose()
