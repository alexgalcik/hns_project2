import random

from Modules.Api.BinanceApi import BinanceApi
from NeuralNetwork.PriceNeuralNetworkEnvironment import PriceNeuralNetworkEnvironment

pair = 'LTCEUR'

shuffle = bool(random.randint(0, 1))
epochs = 2000
train_ratio = 0.99
window_size = 15

print('Window size:', window_size, ' Train ratio:', train_ratio, ' Epochs:', epochs, ' Shuffle: ', shuffle)
priceNeuralNetworkEnvironment = PriceNeuralNetworkEnvironment()
priceNeuralNetworkEnvironment.window_size = window_size
priceNeuralNetworkEnvironment.pair = pair
priceNeuralNetworkEnvironment.load_dataframe()
priceNeuralNetworkEnvironment.preprocess_input_data(train_ratio=train_ratio)
priceNeuralNetworkEnvironment.prepare_train_data()

priceNeuralNetworkEnvironment.prepare_test_evaluation_data()

priceNeuralNetworkEnvironment.create_model()
priceNeuralNetworkEnvironment.train(epochs=epochs, validation_split=0.1, shuffle=shuffle, batch_size=2048,
                                    show_graph=True)
priceNeuralNetworkEnvironment.save_tensorflow_model()
priceNeuralNetworkEnvironment.save_scalers(enabled=True)
priceNeuralNetworkEnvironment.real_test()
binance_handler = BinanceApi()
dataset = binance_handler.get_dataset(hours=30 * 24, interval='60', pair=pair)
dataset.drop(dataset.tail(1).index, inplace=True)
dataset = binance_handler.get_recent_ohlc(dataset, '60', pair)
input_data = priceNeuralNetworkEnvironment.prepare_input(dataset)
output = priceNeuralNetworkEnvironment.model_predict(input_data)
print("output:", output)
