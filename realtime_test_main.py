from Modules.Api.BinanceApi import BinanceApi
from NeuralNetwork.PriceNeuralNetworkEnvironment import PriceNeuralNetworkEnvironment

pair = 'LTCEUR'
window_size = 15
interval = '1d'
priceNeuralNetworkEnvironment = PriceNeuralNetworkEnvironment()
priceNeuralNetworkEnvironment.pair = pair
priceNeuralNetworkEnvironment.load_scaler('2023-02-10-15-22-input-scaler.gz', input_scaler=True)
priceNeuralNetworkEnvironment.load_scaler('2023-02-10-15-22-target-scaler.gz', input_scaler=False)
priceNeuralNetworkEnvironment.load_tensorflow_model('2023-02-10-15-21-LTCEUR')
priceNeuralNetworkEnvironment.window_size = window_size

binance_handler = BinanceApi()
dataset = binance_handler.get_dataset(hours=18 * 24, interval=interval, pair=pair)
dataset.drop(dataset.tail(1).index, inplace=True)
dataset = binance_handler.get_recent_ohlc(dataset, interval, pair)
input_data = priceNeuralNetworkEnvironment.prepare_input(dataset)
output = priceNeuralNetworkEnvironment.model_predict(input_data)
print("output:", output)
